"use strict";
const userService = require("../services/users");
const response = require("../exchange/response");
const userMapper = require("../mappers/user");
let moment = require('moment');

const create = async (req, res) => {
  const log = req.context.logger.start(`api:users:create`);
  try {
    const user = await userService.create(req.body, req.context);
    if (user.err === null || user.err === undefined) {
      const message = "User Resgiter Successfully";
      log.end();
      return response.success(res, message, {});
    } else {
      log.end();
      return response.seeother(res, user.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const login = async (req, res) => {
  const log = req.context.logger.start("api:users:login");
  // console.log('log login ==>>>', log);
  try {
    const user = await userService.login(req.body, req.context);

    log.end();
    return response.authorized(res, userMapper.toLoginModel(user), user.token);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const changePassword = async (req, res) => {
  const log = req.context.logger.start("api:users:changePassword");
  try {
    const message = await userService.changePassword(req.body, req.context);
    log.end();
    return response.success(res, message);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const forgotVerifyEmail = async (req, res, next) => {
  const log = req.context.logger.start(`api:users:forgotVerifyEmail`);
  try {
    const message = await userService.forgotVerifyEmail(req.body, req.context);
    log.end();
    return response.success(res, message);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};



exports.create = create;
exports.login = login;
exports.changePassword = changePassword;
exports.forgotVerifyEmail = forgotVerifyEmail;



