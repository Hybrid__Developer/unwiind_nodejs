module.exports = [
  {
    url: "/register",
    post: {
      summary: "create",
      description: "create",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of user creation",
          required: true,
          schema: {
            $ref: "#/definitions/userCreate"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/login",
    post: {
      summary: "Login user",
      description: "user login into system and get its token to access apis",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of login user",
          required: true,
          schema: {
            $ref: "#/definitions/Login"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/changePassword",
    post: {
      summary: "Change Password",
      description: "change Password",
      parameters: [
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of resetPassword user",
          required: true,
          schema: {
            $ref: "#/definitions/resetPassword"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: '/forgotVerifyEmail',
    post: {
      summary: 'verify email',
      description: 'when user forgot password verify registered email',
      parameters: [{
        in: 'body',
        name: 'body',
        description: 'Model of verify registerd email for forgot password',
        required: true,
        schema: {
          $ref: '#/definitions/verifyEmail'
        }
      }],
      responses: {
        default: {
          description: 'Unexpected error',
          schema: {
            $ref: '#/definitions/Error'
          }
        }
      }
    }
  },
];
