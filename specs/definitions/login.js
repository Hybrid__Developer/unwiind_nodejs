module.exports = [
  {
    name: "Login",
    properties: {
      email: {
        type: "string"
      },
      password: {
        type: "string"
      }
    }
  }
];
