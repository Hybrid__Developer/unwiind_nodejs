module.exports = [
  {
    name: "userDeviceToken",
    properties: {
      deviceToken: {
        type: "string"
      }
    }
  }
];
