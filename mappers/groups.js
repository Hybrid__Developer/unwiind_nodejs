"use strict";

exports.toModel = entity => {

    const model = {
        groupName: entity,
    };
    return model;

};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};
