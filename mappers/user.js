"use strict";

exports.toModel = entity => {
  const model = {
    id: entity.id,
    userName: entity.userName,
    email: entity.email,
  };
  return model;
};
exports.toLoginModel = entity => {
  let login = exports.toModel(entity);
  login.token = entity.token
  return login
};
exports.toSearchModel = entities => {
  return entities.map(entity => {
    return exports.toModel(entity);
  });
};
