"use strict";

const express = require("express");
const appConfig = require("config").get("app");
const logger = require("@open-age/logger")("server");
const path = require('path');
const Http = require("http");
const port = process.env.PORT || appConfig.port || 7001;
var admin = require("firebase-admin");
var serviceAccount = require("./unwiind-firebase.json");
const bcrypt = require("bcrypt");
var bodyParser = require('body-parser');
const { changePassword } = require("./services/users");




const app = express();
var server = Http.createServer(app);

app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});


app.get('/', (req, res) => {
  res.send('app is running nodejs unwiind');
});


app.get('/resetPassword/:token', function (req, res) {

  res.sendFile(path.join(__dirname + '/email_templates/index.html'));
})

app.post("/resetPassword/:token", async function (req, res) {
  console.log('req.body ==>>>', req.body, req.params);
  const { Password } = req.body; console.log("password =====>>>>>>>", Password);
  if (!Password) {
    return res.status(400).json({
      status: false,
      message: "Password is required!",
    });
  } else {

    async function resetPassword(user) {
      user.password = bcrypt.hashSync(Password, 10);
      user.updatedOn = new Date();
      await user.save()
      return res.json({
        status: 200,
        message: "Password changed successfully",
      });
    }
    db.user.findOne({
      resetPasswordToken: req.params.token,
      resetPasswordExpires: { $gt: Date.now() },
    }).then((user) => {
      if (!user) {
        throw new Error("otpVerifyToken is wrong or expired.")
      }
      resetPassword(user);
    })
      .catch((err) => {
        return res.json({
          status: "error",
          // message: "Token Expired!!!",
          message: err.message,
        });
      })
  };
});

const boot = () => {
  const log = logger.start("app:boot");
  log.info(`environment:  ${process.env.NODE_ENV}`);
  log.info("starting server");
  server.listen(port, () => {
    log.info(`listening on port: ${port}`);
    log.end();
  });
};

const init = async () => {
  await require("./settings/database").configure(logger);
  await require("./settings/express").configure(app, logger);
  await require("./settings/routes").configure(app, logger);
  // await require('./jobs/taskNotification').schedule(logger)
  app.get('/term&policies', function (req, res) {
    res.sendFile(__dirname + '/templates/term&policies.html');
  });
  boot();
};
init();
