const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
// const imageUrl = require('config').get('image').url
var nodemailer = require('nodemailer')
var moment = require('moment');
const crypto = require("crypto");


const buildUser = async (model, context) => {
  const { userName, email, password } = model;
  const log = context.logger.start(`services:users:build${model}`);
  let userModel = {
    userName: userName.toLowerCase(),
    email: email.toLowerCase(),
    password: password,
    createdOn: new Date(),
    updatedOn: new Date()
  }
  const user = await new db.user(userModel).save();
  log.end();
  return user;
};

const create = async (model, context) => {
  const log = context.logger.start("services:users:create");
  const isEmail = await db.user.findOne({ email: model.email.toLowerCase() });
  if (isEmail) {
    return {
      err: "Email already resgister"
    }
  }
  model.password = encrypt.getHash(model.password, context);
  model.password = (model.password && model.password !== undefined && model.password !== null && model.password !== 'string') ? model.password : encrypt.getHash(model.firstName.toLowerCase().replace(/\s+/g, '') + '@1234', context)

  const user = await buildUser(model, context);
  user.save()
  log.end();
  return user;
};



const login = async (model, context) => {
  const log = context.logger.start("services:users:login");

  const query = {};

  if (model.email) {
    query.email = model.email;
  }

  let user = await db.user.findOne(query)

  if (!user) {
    log.end();
    throw new Error("user not found");
  }
  const isMatched = encrypt.compareHash(model.password, user.password, context);

  if (!isMatched) {
    log.end();
    throw new Error("password mismatch");
  }

  const token = auth.getToken(user.id, false, context);
  user.token = token;
  user.updatedOn = new Date();
  user.save();
  log.end();
  return user;
};


const changePassword = async (model, context) => {
  const log = context.logger.start(`service/users/changePassword: ${model}`);
  const user = context.user;
  const isMatched = encrypt.compareHash(
    model.oldPassword,
    user.password,
    context
  );
  if (isMatched) {
    const newPassword = encrypt.getHash(model.newPassword, context);
    user.password = newPassword;
    user.updatedOn = new Date();
    await user.save();
    log.end();
    return "Password Updated Successfully";
  } else {
    log.end();
    throw new Error("Old Password Not Match");
  }
};


const forgotVerifyEmail = async (model, context) => {
  const log = context.logger.start('service/users/forgotVerifyEmail');

  const reset = {};
  const resetPasswordToken = crypto.randomBytes(20).toString("hex");
  const resetPasswordExpires = Date.now() + 3600000; //expires in an hour

  reset.resetPasswordToken = resetPasswordToken;
  reset.resetPasswordExpires = resetPasswordExpires;

  async function storedToken() {
    await db.user.findOneAndUpdate({ email: model.email }, { $set: reset }, { new: true }).then((tkn) => {
      console.log("reset password token is stored in db", tkn);
    });
  }

  let user = await db.user.findOne({ email: model.email })

  if (!user) {
    log.end();
    throw new Error("The email address " + model.email + " is not associated with any account. Please check your email address and try again.");
  } else {
    storedToken();


    let smtpTransport = nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: {
        user: '4539381fce9237',
        pass: '8b3c08ba468564'
      }
    });


    if (user) {
      // const link = 'http://' + req.headers.host + '/reset/' + resetPasswordToken;
      const link = 'http://93.188.167.68:7001/resetPassword/' + resetPasswordToken;


      let mailOptions = {
        from: "smtp.mailtrap.io",
        to: `${user.email}`,
        subject: "Link To Reset Password",
        html: "Hi " +
          "<b style='text-transform: capitalize;'>" + `${user.userName}` + "</b>" +
          "<br>  Please Click on the following link to verify your email to reset password. This is valid for one hour of receiving it.<br><a href=" +
          link +
          ">Click here to verify</a> <br> 'If you did not request this, please ignore this email and your password will remain unchanged.'",
      };
      smtpTransport.sendMail(mailOptions, function (error, info) {
        if (!error) {
          log.end();
          return "Recovery email sent"
        } else {
          log.end();
          return error.message
        }
      });
    }
  }

};


exports.create = create;
exports.login = login;
exports.changePassword = changePassword;
exports.forgotVerifyEmail = forgotVerifyEmail;

